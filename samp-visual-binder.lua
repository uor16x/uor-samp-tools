local imgui = require 'mimgui'
imgui.ShowCursor = true
local ffi = require 'ffi'
local vkeys = require 'vkeys'
local encoding = require 'encoding'
local inicfg = require 'inicfg'
encoding.default = 'CP1251'
local u8 = encoding.UTF8
local wm = require 'windows.message'
local new, str, sizeof = imgui.new, ffi.string, ffi.sizeof
local sizeX, sizeY = getScreenResolution()

local vars_ini_path = 'samp-visual-binder-vars.ini'
local vars_ini = inicfg.load({ vars = {} }, vars_ini_path)
local vars = vars_ini.vars

local popups_ini_path = 'samp-visual-binder-popups.ini'
local popups = inicfg.load(nil, popups_ini_path)

function inistr(str)
  return str:gsub('"','')
end

local popups_data = {}
if popups ~= nil then
  for section, data in pairs(popups) do
    if data.is_popup == true then
      local curr_popup = {
        key = vkeys[data.key],
        is_rendering = false,
        allow_execute = true,
      }
      local text_options = popups[string.format('%s_text_opts', section)]
      curr_popup.text_options = {}
      if text_options ~= nil then
        for opt_index = 1, text_options.length do
          local curr_opt = {
            hint = inistr(u8:encode(text_options[string.format('option%s', opt_index)])),
            commands = {}
          }
          for cmd_index = 1, text_options[string.format('option%s_length', opt_index)] do
            local cmd = inistr(text_options[string.format('option%s_cmd%s', opt_index, cmd_index)])
            table.insert(curr_opt.commands, cmd)
          end
          table.insert(curr_popup.text_options, curr_opt)
        end
      end

      if data.combo ~= nil then
        curr_popup.combo = {}
        for combo_index = 1, data.combo do
          local curr_combo_ini = popups[string.format('%s_combo%s', section, combo_index)]
          local curr_combo = {
            name = curr_combo_ini.name,
            variable = inistr(u8:encode(curr_combo_ini.variable)),
            selected = new.int(0),
            width = curr_combo_ini.width,
            options = {}
          }
          for curr_combo_option_index = 1, tonumber(curr_combo_ini.options) do
            local curr_combo_opt = inistr(u8:encode(curr_combo_ini[string.format('option%s', curr_combo_option_index)]))
            table.insert(curr_combo.options, curr_combo_opt)
          end
          vars[curr_combo.variable] = inistr(u8:decode(curr_combo.options[1]))
          table.insert(curr_popup.combo, curr_combo)
        end
        inicfg.save(vars_ini, vars_ini_path)
      end

      if data.slider ~= nil then
        curr_popup.slider = {}
        for slider_index = 1, data.slider do
          local curr_slider_ini = popups[string.format('%s_slider%s', section, slider_index)]
          local curr_slider = {
            name = curr_slider_ini.name,
            variable = inistr(u8:encode(curr_slider_ini.variable)),
            selected = new.int(curr_slider_ini.from),
            from = curr_slider_ini.from,
            to = curr_slider_ini.to
          }
          vars[curr_slider.variable] = curr_slider.selected[0]
          table.insert(curr_popup.slider, curr_slider)
        end
        inicfg.save(vars_ini, vars_ini_path)
      end

      curr_popup.frame = imgui.OnFrame(
        function()
          return curr_popup.is_rendering
        end,
        function(player)
          local needSave = false
          imgui.SetNextWindowPos(imgui.ImVec2(sizeX / 2 - 450, sizeY / 2 - 200), imgui.Cond.Appearing, imgui.ImVec2(0.5, 0.5))
          imgui.SetNextWindowSize(imgui.ImVec2(50, 1), imgui.Cond.Appearing)
          imgui.PushStyleColor(imgui.Col.WindowBg, imgui.ImVec4(0.11, 0.15, 0.17, 0.5))
          imgui.Begin(
            section,
            new.bool(curr_popup.is_rendering),
            imgui.WindowFlags.NoResize
              + imgui.WindowFlags.NoCollapse
              + imgui.WindowFlags.NoMove
              + imgui.WindowFlags.NoTitleBar
              + imgui.WindowFlags.NoScrollbar
              + imgui.WindowFlags.AlwaysAutoResize
          )

          if curr_popup.combo ~= nil then
            for index, combo in ipairs(curr_popup.combo) do
              if combo.width then
                imgui.SetNextItemWidth(combo.width)
              end
              if imgui.Combo(
                string.format('< %s', inistr(u8:encode(combo.name))),
                combo.selected,
                new['const char*'][#combo.options](combo.options),
                #combo.options
              )
              then
                vars[combo.variable] = inistr(u8:decode(combo.options[combo.selected[0]+1]))
                needSave = true
              end
            end
          end

          if curr_popup.slider ~= nil then
            for index, slider in ipairs(curr_popup.slider) do
              if imgui.SliderInt(
                string.format('< %s', inistr(u8:encode(slider.name))),
                slider.selected,
                slider.from,
                slider.to
              )
              then
                vars[slider.variable] = slider.selected[0]
                needSave = true
              end
            end
          end
          
          for index, option in ipairs(curr_popup.text_options) do
            imgui.Text(string.format('[%s] %s', index, option.hint))
          end
          
          if needSave then
            inicfg.save(vars_ini, vars_ini_path)
          end
          imgui.End()
        end
    )

      popups_data[section] = curr_popup
    end
  end
end

function dump(o)
  if type(o) == 'table' then
     local s = '{ '
     for k,v in pairs(o) do
        if type(k) ~= 'number' then k = '"'..k..'"' end
        s = s .. '['..k..'] = ' .. dump(v) .. ','
     end
     return s .. '} '
  else
     return tostring(o)
  end
end

function isValidKeyPress()
	return sampIsChatInputActive() and not sampIsDialogActive() and not isPauseMenuActive() and not isSampfuncsConsoleActive()
end

function checkPopup(popup)
  if not isKeyDown(popup.key) and (popup.is_rendering or not popup.allow_execute) then
    popup.allow_execute = true
    popup.is_rendering = false
  end
  if isKeyDown(popup.key) and popup.allow_execute and not isValidKeyPress() then
    popup.is_rendering = true
    if #popup.text_options > 0 then
      for index, option in ipairs(popup.text_options) do
        if isKeyJustPressed(vkeys[string.format("VK_%s", index)]) then
          lua_thread.create(function ()
            for cmd_index = 1, #popup.text_options[index].commands do
              send(popup.text_options[index].commands[cmd_index])
              if cmd_index ~= #popup.text_options[index].commands then
                wait(1200)
              end
            end
          end)
          popup.is_rendering = false
          popup.allow_execute = false
        end
      end
    end
  end
end

function setVar(args)
  function sendUsage()
    sampAddChatMessage(string.format(' {FFFFFF}Usage: /addvar name value', name, vars[name]))
  end
  if #args == 0 then
    sendUsage()
  else
    argTable = {}
    for arg in args:gmatch("%S+") do
      table.insert(argTable, arg)
    end
    name = argTable[1]
    value = argTable[2]
    if not name or not value then
     sendUsage()
    else
      vars[name] = value
      if inicfg.save(vars_ini, vars_ini_path) then
        sampAddChatMessage(string.format(' {FFFFFF}[Variable] %s = %s', name, vars[name]))
      end
    end
  end
end

function getVar(name)
  if #name == 0 then
    sampAddChatMessage(string.format(' {FFFFFF}Usage: /getvar name', name, vars[name]))
  else
    sampAddChatMessage(string.format(' {FFFFFF}[Variable] %s = %s', name, vars[name]))
  end
end

function send(msg)
  transformed = string.gsub(msg, '<P>(.-)</P>', function (param)
    if not vars[param] then
      print(string.format('No such variable - %s', param))
      return '-'
    else
      return vars[param]
    end
  end)
  sampSendChat(transformed)
end

function openChatT()
  if isKeyDown(vkeys.VK_T) and wasKeyPressed(vkeys.VK_T) and not sampIsChatInputActive() and not sampIsDialogActive() then
    sampSetChatInputEnabled(true)
  end
end

function reload()
  print('reloading...')
  thisScript():reload()
end

function main()
  print('started')
  sampRegisterChatCommand("setvar", setVar)
  sampRegisterChatCommand("getvar", getVar)
  sampRegisterChatCommand("svbreload", reload)
  while true do
    openChatT()
    for name, popup in pairs(popups_data) do
      checkPopup(popup)
    end
    wait(0)
  end
end